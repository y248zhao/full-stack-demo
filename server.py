import psycopg2
import json

from flask import Flask, render_template, request, url_for, redirect
from requests_futures.sessions import FuturesSession

app = Flask(__name__)
cache = {}

@app.route("/")
def home():
    return render_template('index.html')

@app.route('/hotels', methods=["POST"])
def hotels():
    city = request.form['city']
    checkin = request.form['checkin']
    checkout = request.form['checkout']
    data = {
        'city': city,
        'checkin': checkin,
        'checkout': checkout,
        'provider': 'snaptravel'
    }
    if ((city, checkin, checkout)) in cache:
        result = cache[(city, checkin, checkout)]
    else:
        result = find_available_hotel(data)
        cache[(city, checkin, checkout)] = result
    return render_template('hotel.html', result=result)

def find_available_hotel(data):
    hotels = []
    if not hotels:
        session = FuturesSession()
        req_one = session.post('https://experimentation.getsnaptravel.com/interview/hotels', data=data)
        req_two = session.post('https://experimentation.getsnaptravel.com/interview/hotels', data=data)
        snaptravel_prices = req_one.result()
        competitor_prices = req_two.result()
        merged_prices = {}
        for hotel in snaptravel_prices.json().get('hotels',[]):
            merged_prices[hotel['id']] = hotel
        for hotel in competitor_prices.json().get('hotels', []):
            if hotel['id'] in merged_prices:
                if merged_prices[hotel['id']]['price'] > hotel['price']:
                    # If snaptravel price is not as good, remove the listing
                    del merged_prices[hotel['id']]
                else:
                    merged_prices[hotel['id']]['competitor_price'] = hotel['price']
            else:
                merged_prices[hotel['id']] = hotel
    return merged_prices.values()
